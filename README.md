# Analysing flow cytometry data with Python
Welcome to the Cytometric Data Analysis Project! This Notebook is a part of the article '**Design and quantification of co-cultures combining heterotrophic yeast with phototrophic sugar-secreting cyanobacteria**' by Hasenklever *et al.*
 
This project is designed to facilitate the reading, processing, and analysis of flow cytometry data stored in Flow Cytometry Standard (FCS) files. We show how to use two available open source Python packages that allow us to parse FCS files, extract metadata, and analyze cytometric data.

## Content
- mixed_culture.fcs: exemplary data file used for the analysis,
- fcs_with_FlowCytometryTools.ipynb: notebook showing base data processing with the [FlowCytometryTools](https://eyurtsev.github.io/FlowCytometryTools/) package,
- fcs_with_FlowCytometryTools.ipynb: notebook showing base data processing with the [FlowKit](https://github.com/whitews/FlowKit) package,
- README.md: top-level description for future users,
- requirements.txt: The requirements file for reproducing the analysis environment.

## Features
Both notebooks will allow you to perform following tasks:

- FCS File Parsing: Read and extract information from FCS files, including metadata and measurement data.
- Metadata Handling: Access and interpret key metadata stored in the FCS text segment.
- Density Plot: Generates a 2D density plot for the first two channels.
- Histrogram Plot: Creates a Histrogram plot to visualize the cell intensity distributions across multiple samples.
- Scatter plot : Generates a 2D Scatter plot of selected channels, including gating.

## Prerequisites

Before running the code, ensure you have the following packages installed:

- flowCal
- pandas
- matplotlib
- numpy
- os
- flowcytometrytools
- flowkit
- bokeh.

#### About the Main Project
This repository supports the flow cytometry data processing in the spirit of FAIR and open science where we show how open source software can be used to process the raw FCS files.

With the increasing demand for sustainable biotechnologies, mixed consortia containing a phototrophic microbe and heterotrophic partner species are being explored as a method for solar-driven bioproduction. One approach involves the use of CO2-fixing cyanobacteria that secrete organic carbon to support the metabolism of a co-cultivated heterotroph, which in turn transforms the carbon to higher-value goods or services. Single-cell flow cytometry is one of the basic laboratory techniques for co-culture monitoring and within this reporsitory and we provide examples of two non proprietary software to use for data analysis of raw FCS files in line with FAIR principles.

### Contact
If you have any questions or need further assistance, please open an issue in the repository or contact the [project maintainer](tanvir.hassan@rwth-aachen.de).




